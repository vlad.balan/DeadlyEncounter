using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class UpgradesScript : MonoBehaviour
{
    public ItemsScript itemsData;
    public int lvl=0;
    TextMeshProUGUI lvlText;
    private GameObject Player;

    private void Awake()
    {
        lvlText = GetComponentInChildren<TextMeshProUGUI>();
    }

    private void LateUpdate()
    {
        lvlText.text = "Lvl : " + (lvl);
    }

    public void OnClickUpgrade()
    {
        this.transform.parent.parent.gameObject.SetActive(false);
        itemsData.lvl = lvl;
        switch (itemsData.ItemType)
        {
            case ItemsScript.ItemTypes.FireDamage:

                GameObject.FindGameObjectWithTag("Player").transform.GetChild(1).GetComponent<Weapon>().Init(itemsData);

                break;

            case ItemsScript.ItemTypes.FireCount:

                GameObject.FindGameObjectWithTag("Player").transform.GetChild(1).GetComponent<Weapon>().Init(itemsData);

                break;

            case ItemsScript.ItemTypes.FireRange:

                GameObject.FindGameObjectWithTag("Player").transform.GetChild(1).GetComponent<Weapon>().Init(itemsData);

                break;

            case ItemsScript.ItemTypes.FireSpeed:

                GameObject.FindGameObjectWithTag("Player").transform.GetChild(1).GetComponent<Weapon>().Init(itemsData);

                break;

            case ItemsScript.ItemTypes.PlayerSpeed:

                GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerScript>().SetGear(itemsData);

                break;


        }

        lvl++;

        if(lvl == itemsData.Value.Length)
        {
            GetComponent<Button>().interactable = false;
        }
    }


}
