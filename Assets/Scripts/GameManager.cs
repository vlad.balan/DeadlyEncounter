using PlayFab.ClientModels;
using PlayFab;
using TMPro;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance { get; set; }
    [Header("Game Info")]
    public float gameTime;
    public bool isLive = false;


    [Header("Player Stats")]
    public int level = 0;
    public int kill = 0;
    public int exp = 0;
    public int[] nextLvl = { 10, 30, 60, 100, 150, 210, 280 };
    public int Coins = 0;


    public PlayerScript player;
    public ItemsScript freshStart;
    bool firstTime = true;

    public TextMeshProUGUI score;
    public GameObject GameOverPanel;
    public GameObject upgradeShow;
    public TextMeshProUGUI coinText;
    public TextMeshProUGUI coinTextGameOver;

    private void Awake()
    {
        instance = this;
        upgradeShow.SetActive(false);

    }

    void Update()
    {
        if (isLive)
        {
            
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerScript>();
            if(firstTime)
            {
                player.transform.GetChild(1).GetComponent<Weapon>().Init(freshStart);
                firstTime = false;
            }
            
            gameTime += Time.deltaTime;
        }

    }



    public void GetExp()
    {
        
        exp += 1;

        if (exp == nextLvl[level])
        {
            upgradeShow.SetActive(true);
            
            level++;
            exp = 0;
        }
    }
    public GameObject playerPref;

    public void SetScore()
    {
        score.text = "Score: " + kill.ToString();
        coinTextGameOver.text = "Score: " + Coins.ToString();
    }


    public void GameOver()
    {
        upgradeShow.SetActive(false);
        MainMenuScript.instance.GameOverExit();
        isLive = false;
        Spawner.instance.spawnOn_Off = false;

        Spawner.instance.DeleteAllEnemies();

        Destroy(GameObject.FindGameObjectWithTag("Player"));

        PlayfabManager.Instance.SendBestScores(kill);
        gameTime = 0;
        kill = 0;
        exp = 0;
        level = 0;
        firstTime = true;
        PlayFabClientAPI.AddUserVirtualCurrency(new AddUserVirtualCurrencyRequest()
        {
            Amount = Coins,
            VirtualCurrency = "MN",
        }, null,
        (error) =>
        {
            Debug.Log(error);
        });

        Camera.main.transform.position = new Vector3(0, 0, -17);
        Coins = 0;
    }
    
    public void GameStart()
    {

        resetUpgrades();
        MainMenuScript.instance.StartGame();
        Instantiate(playerPref);
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerScript>();
        isLive = true;
        Spawner.instance.spawnOn_Off = true;
        
    }
    public GameObject items;
    public void resetUpgrades()
    {
        foreach (UpgradesScript data in items.GetComponentsInChildren<UpgradesScript>())
        {
            data.lvl = 0;
        }
    }

    public void SetCoins(int value)
    {
        Coins += value;
    }


}
