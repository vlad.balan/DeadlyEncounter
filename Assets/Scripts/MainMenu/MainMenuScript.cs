using PlayFab.MultiplayerModels;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuScript : MonoBehaviour
{
    public static MainMenuScript instance { get; set; }
    [Header("Panels")]
    private GameObject mainMenuPanel;
    public GameObject canvas;
    public GameObject GameOver;
    public GameObject LeaderBoard;
    public GameObject Shop;
    public GameObject Register;
    public GameObject Login;

    public TextMeshProUGUI PlayerName;


    void Start()
    {
        instance = this;
        mainMenuPanel = GameObject.FindGameObjectWithTag("MainMenuPanel");

        canvas.SetActive(false);
        mainMenuPanel.SetActive(false);
        LeaderBoard.SetActive(false);
        Shop.SetActive(false);
        Register.SetActive(false);
        Login.SetActive(true);
    }

    public void StartGame()
    {
        this.gameObject.SetActive(false);
        canvas.SetActive(true);
    }

    public void GameOverExit()
    {
        GameOver.SetActive(false);
        canvas.SetActive(false);
        this.gameObject.SetActive(true);
        mainMenuPanel.SetActive(true);
        
    }


    public void ClickLeaderBoard()
    {
        LeaderBoard.SetActive(true);
        PlayfabManager.Instance.LeaderBoardGetData();
        mainMenuPanel.SetActive(false);
    }

    public void ClickBackFromLeaderBoard()
    {
        LeaderBoard.SetActive(false);
        mainMenuPanel.SetActive(true);
    }

    public void ClickShopButton()
    {
        PlayfabManager.Instance.GetCoins();
        Shop.SetActive(true);
        mainMenuPanel.SetActive(false);
    }

    public void clickBackFromTheShop()
    {
        Shop.SetActive(false);
        mainMenuPanel.SetActive(true);
        
    }

    public void RegisterButton()
    {
        Register.SetActive(true);
        Login.SetActive(false);

    }

    public void registerSuccessful()
    {
        Register.SetActive(false);
        Login.SetActive(true);
    }

    public void clickBackFromTheRegister()
    {
        Register.SetActive(false);
        Login.SetActive(true);

    }

    public void LoginButton()
    {
        mainMenuPanel.SetActive(true);
        Login.SetActive(false);

    }


}
