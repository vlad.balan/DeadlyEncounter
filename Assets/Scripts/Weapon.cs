using System.Collections;
using System.Collections.Generic;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Netcode;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public float damage;
    public float cnt;
    public float speed;
    public float range;
    public bool isAlive =false;

    private void Awake()
    {
        isAlive = true;
    }
    void Update()
    {
        if(isAlive)
        {
            transform.Rotate(Vector3.back * speed * Time.deltaTime);
            WeaponRoationAndCount();
        }
    }

    public void Init(ItemsScript itemData)
    {
        Debug.Log("ItemData id : " + itemData.id);
        switch (itemData.id)
        {
            case 0:
                speed = itemData.baseSpeed;
                range = itemData.baseRange;
                damage = itemData.baseDamage;
                cnt = itemData.baseCount;
                break;

            case 1:
                damage = itemData.Value[itemData.lvl];
                break;
            case 2:
                range = itemData.Value[itemData.lvl];
                break;
            case 3:
                cnt = itemData.Value[itemData.lvl];
                break;

            case 5:
                speed = itemData.Value[itemData.lvl];
                break;

            default:
                break;
        }
    }

    public int CheckGear()
    {
        foreach(SkinsData skin in PlayfabManager.Instance.skinsdata)
        {
            if(skin.isEquiped && skin.skintype == SkinsData.skinType.Weapon)
            {
                return skin.id;
            }
        }

        return 1;
    }

    void WeaponRoationAndCount()
    {
        
        for (int i=0; i < cnt; i++)
        {
            Transform bullet;

            if(i < transform.childCount)
            {
                bullet = transform.GetChild(i);
            }
            else
            {
                bullet = PoolManager.instance.Get(CheckGear()).transform;
                bullet.parent = transform;
            }

            bullet.localPosition = Vector3.zero;
            bullet.localRotation = Quaternion.identity;

            Vector3 rotation = Vector3.forward * 360 * i / cnt;
            bullet.Rotate(rotation);
            bullet.Translate(bullet.right * range, Space.World);
            bullet.GetComponent<Bullet>().Init(damage);
        }
    }

    

}
