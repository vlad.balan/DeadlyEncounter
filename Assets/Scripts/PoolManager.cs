using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolManager : MonoBehaviour
{
    public static PoolManager instance { get; private set; }
    public GameObject[] prefabs;

    [SerializeField]public List<GameObject>[] pools;

    private void Start()
    {
        instance = this;
    }

    private void Awake()
    {
        pools = new List<GameObject>[prefabs.Length];

        for (int i = 0;i< pools.Length; i++)
        {
            pools[i] = new List<GameObject>();
        }
    }

    public GameObject Get(int i)
    {
        GameObject select = null;

        select = Instantiate(prefabs[i], transform);
        return select;
    }


}
