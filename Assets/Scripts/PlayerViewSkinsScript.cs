using PlayFab.EconomyModels;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerViewSkinsScript : MonoBehaviour
{

    public UnityEngine.UI.Image player;
    public UnityEngine.UI.Image Weapon1;
    public UnityEngine.UI.Image Weapon2;
    void Update()
    {
        foreach (SkinsData data in PlayfabManager.Instance.skinsdata)
        {
            if (data.isEquiped && data.skintype == SkinsData.skinType.Weapon)
            {
                Weapon1.sprite = data.sprite;
                Weapon2.sprite = data.sprite;
            }
            else if (data.isEquiped && data.skintype == SkinsData.skinType.Player)
            {
                player.sprite = data.sprite;
            }
        }
    }

    public void setSkins(string playerSkinId,string weaponSkinId)
    {
        foreach (SkinsData data in PlayfabManager.Instance.skinsdata)
        {
            if (data.id.ToString() == playerSkinId  && data.skintype == SkinsData.skinType.Player)
            {
                Debug.Log("PlayerSkin");
                data.isEquiped = true;
            }
            else if (data.id.ToString() == weaponSkinId && data.skintype == SkinsData.skinType.Weapon)
            {
                Debug.Log("WeaponSkin");
                data.isEquiped = true;
            }
        }
    }
    string playerskinId;
    string weaponskinId;
    public void saveSkins()
    {
        foreach (SkinsData data in PlayfabManager.Instance.skinsdata)
        {
            if (data.isEquiped && data.skintype == SkinsData.skinType.Player)
            {
                Debug.Log("Player = " + data.id);
                playerskinId = data.id.ToString();
            }
            else if (data.isEquiped && data.skintype == SkinsData.skinType.Weapon)
            {
                Debug.Log("Weapon " + data.id);
                weaponskinId = data.id.ToString();
            }

        }
        PlayfabManager.Instance.SavePlayerSkinData(playerskinId, weaponskinId);
    }
}
