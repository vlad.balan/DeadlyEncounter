using Unity.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Unity.Netcode;

public class PlayerUI : MonoBehaviour
{

    public TextMeshProUGUI LvlText;
    public TextMeshProUGUI killsText;
    public TextMeshProUGUI coinsText;
    public TextMeshProUGUI gameTimer;

    public Slider expSlider;
    public Slider HealthSlider;

    private void LateUpdate()
    {
        LvlAndExp();
        kills();
        CoinsGet();
        GameTimer();
        PlayerHealth();
    }

    private void LvlAndExp()
    {
        float curExp = GameManager.instance.exp;
        float maxExp = GameManager.instance.nextLvl[GameManager.instance.level];
        expSlider.value = curExp / maxExp;
        LvlText.text = "LVL:" + GameManager.instance.level.ToString();
    }

    private void kills()
    {
        killsText.text = GameManager.instance.kill.ToString();
    }
    private void CoinsGet()
    {
        coinsText.text = GameManager.instance.Coins.ToString();
    }

    private void GameTimer()
    {
        float time = GameManager.instance.gameTime;
        gameTimer.text = string.Format("{0:D2}:{1:D2}", Mathf.FloorToInt(time / 60), Mathf.FloorToInt(time % 60));

    }

    private void PlayerHealth()
    {
        try
        {
            HealthSlider.value = PlayerScript.instance.health/PlayerScript.instance.maxHealth;
        }
        catch { }
    }

}
