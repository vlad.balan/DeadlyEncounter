using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;
using static UnityEngine.GraphicsBuffer;
using TMPro;
using Unity.Collections;
using Newtonsoft.Json.Bson;
using Unity.Services.Lobbies.Models;
using Unity.VisualScripting;
using UnityEngine.InputSystem.XR;

public class PlayerScript : MonoBehaviour
{
    public static PlayerScript instance { get; private set; }

    public float moveSpeed = 5f;
    public Joystick joystick;
    public Camera mainCamera;
    public float cameraFollowSpeed = 2f;
    private SpriteRenderer spriteRenderer;
    public Rigidbody2D rb;
    private Animator animator;
    public bool isAlive = true;
    public float health;
    public float maxHealth=100;


    void Start()
    {
        instance = this;
        health = maxHealth;
    }


    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        joystick = FindObjectOfType<Joystick>();
        mainCamera = Camera.main;
        
        foreach(SkinsData data in PlayfabManager.Instance.skinsdata)
        {
            if(data.isEquiped && data.skintype == SkinsData.skinType.Player)
            {
                animator.runtimeAnimatorController = data.animator;
            }
        }
        
    }


    void FixedUpdate()
    {
        if (isAlive)
        {
            Vector2 movement = new Vector2(joystick.Horizontal, joystick.Vertical) * moveSpeed;


            rb.velocity = movement;

            if (movement.x > 0)
            {
                spriteRenderer.flipX = false;
            }
            else if (movement.x < 0)
            {
                spriteRenderer.flipX = true;
            }

            animator.SetFloat("Speed", movement.magnitude);
        }
        Vector3 targetPosition = transform.position;
        targetPosition.z = mainCamera.transform.position.z;
        mainCamera.transform.position = Vector3.Lerp(mainCamera.transform.position, targetPosition, cameraFollowSpeed * Time.deltaTime);
    }

    public void SetGear(ItemsScript itemsData)
    {
        switch (itemsData.id)
        {
            case 6:
                moveSpeed = itemsData.Value[itemsData.lvl];
                break;

            case 7:
                break;

        }
    }

    IEnumerator GameOverWait()
    {
        
        isAlive = false;
        animator.SetTrigger("Dead");
        this.gameObject.transform.GetChild(1).gameObject.SetActive(false);
        GameManager.instance.SetScore();

        yield return new WaitForSeconds(1f);
        MainMenuScript.instance.GameOver.SetActive(true);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!collision.gameObject.CompareTag("Enemy") || !isAlive)
        {
            return;
        }
        else
        {
            
            health -= collision.gameObject.GetComponent<Enemy>().damage;

            if (health <= 0)
            {
                rb.velocity = Vector3.zero;
                gameObject.GetComponent<CapsuleCollider2D>().isTrigger = true;
                isAlive = false;
                StartCoroutine(GameOverWait());

            }
        }
    }
}
