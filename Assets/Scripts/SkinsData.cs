using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;
using TMPro;
using UnityEngine.UI;

public class SkinsData : MonoBehaviour
{
    public enum skinType{Player,Weapon};
    public skinType skintype;
    public string ItemName;
    public int id;
    public Sprite sprite;
    public RuntimeAnimatorController animator;
    public TextMeshProUGUI priceText;
    public int price;

    public bool isEquiped = false;
    public bool isBought = false;

    public Button Buy;
    public Button Equip;

    private void Start()
    {
        
    }

    private void Update()
    {
        priceText.text = price.ToString();
        if(isBought == true)
        {
            Buy.gameObject.SetActive(false);
            Equip.gameObject.SetActive(true);
        }
        else
        {
            Buy.gameObject.SetActive(true);
            Equip.gameObject.SetActive(false);
        }

        if(isEquiped)
        {
            Equip.interactable = false;
        }
        {
            Equip.interactable = true;
        }
    }
   
    public void ClickEquip()
    {
        Debug.Log("Equiped");
        
        isEquiped = true;
        Equip.interactable = false;
        foreach(SkinsData data in PlayfabManager.Instance.skinsdata)
        {
            if(data.id != this.id && data.skintype == this.skintype)
            {
                data.isEquiped = false;
            }
        }
        


    }


}
