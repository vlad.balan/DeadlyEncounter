using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem.Processors;

public class Enemy : MonoBehaviour
{

    public float speed;
    public float health;
    public float maxHealth;
    public float damage;
    public int coins;
    public RuntimeAnimatorController[] animController;
    public RuntimeAnimatorController contr;
    public Rigidbody2D player = null;

    bool isAlive;

    Rigidbody2D rb;
    Animator anim;
    SpriteRenderer spriteRenderer;
    WaitForFixedUpdate wait;
    Collider2D coll;

    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        wait = new WaitForFixedUpdate();
        coll = GetComponent<Collider2D>();
        
    }


    void FixedUpdate()
    {
        if (!isAlive || anim.GetCurrentAnimatorStateInfo(0).IsName("Hit"))
        {
            return;
        }
        else
        {
            FollowPlayer();
        }
    }

    private void FollowPlayer()
    {
        Vector2 dirVec = player.position - rb.position;
        rb.MovePosition(rb.position + dirVec.normalized * speed * Time.fixedDeltaTime);
        rb.velocity = Vector2.zero;
    }

    private void LateUpdate()
    {
        if (!isAlive && !player)
        {
            return;
        }
        else
        {
            spriteRenderer.flipX = player.position.x < rb.position.x;
        }
    }

    private void OnEnable()
    {
        
        player = GameManager.instance.player.GetComponent<Rigidbody2D>();
        isAlive = true;
        health = maxHealth;
        spriteRenderer.sortingOrder = 2;
        rb.simulated = true;
        coll.enabled = true;       
    }

    public void Init(SpawnData data)
    {
        speed = data.speed;
        maxHealth = data.health;
        health = data.health;
        damage = data.damage;
        coins = data.coins;
        gameObject.transform.localScale = new Vector3(data.size,data.size,1);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if(!collision.CompareTag("Bullet") || !isAlive)
        {
            return;
        }
        else
        {
            health -= collision.GetComponent<Bullet>().damage;

            StartCoroutine(KnockBack());

            if(health > 0)
            {
                //player = GameManager.instance.player.GetComponent<Rigidbody2D>();
                anim.SetTrigger("Hit");
            }
            else
            {
                isAlive = false;
                spriteRenderer.sortingOrder = 0;
                rb.simulated = false;
                coll.enabled = false;
                GameManager.instance.kill++;
                GameManager.instance.GetExp();
                GameManager.instance.SetCoins(coins);
                Destroy(gameObject);
            }
        }
    }

    IEnumerator KnockBack()
    {
        yield return wait;

        Vector3 playerPosition = GameManager.instance.player.transform.position;
        Vector3 dirVector = transform.position - playerPosition;
        rb.AddForce(dirVector.normalized * 10, ForceMode2D.Impulse);
    }



}
