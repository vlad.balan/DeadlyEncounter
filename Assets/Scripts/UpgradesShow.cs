using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class UpgradesShow : MonoBehaviour
{
    UpgradesScript[] items;

    private void Awake()
    {
        items = GetComponentsInChildren<UpgradesScript>(true);
        RandomUpgrades();
    }

    public void RandomUpgrades()
    {
        foreach(UpgradesScript item in items)
        {
            item.gameObject.SetActive(false);
        }

        int[] random = new int[3];
        while(true)
        {
            random[0] = Random.Range(0, 1);
            random[1] = Random.Range(2, 3);
            random[2] = Random.Range(4, 5);

            if (random[0] != random[1] && random[1] != random[2] && random[0] != random[2])
            {
                break;
            }

        }

        for(int i=0;i<random.Length;i++)
        {
            items[random[i]].gameObject.SetActive(true);
        }

    }
}
