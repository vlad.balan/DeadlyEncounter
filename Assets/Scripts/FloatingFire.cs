using System.Collections.Generic;
using UnityEngine;

public class FloatingFire : MonoBehaviour
{
    public GameObject firePrefab;
    public int numberOfFires;
    public float radius;
    public float speed;

    public bool levelUp = false;

    private List<GameObject> melee  = new List<GameObject>();
    private float angle;

    void Start()
    {
        angle = 360f / numberOfFires;

       UpdateMeleeNumberAndRadius(numberOfFires, radius);
    }

    void Update()
    {
        foreach (GameObject fire in melee)
        {
            fire.transform.RotateAround(transform.position, Vector3.forward, speed * Time.deltaTime);
        }

        if(levelUp)
        {
            UpdateMeleeNumberAndRadius(0, radius);
            UpdateMeleeNumberAndRadius(numberOfFires + 1, radius);
            levelUp = false;
        }
    }

    private void UpdateMeleeNumberAndRadius(int numberOfFires, float radius)
    {
        for (int i = 0; i < numberOfFires; i++)
        {
            GameObject fire = Instantiate(firePrefab, transform.position, Quaternion.identity);
            fire.transform.SetParent(transform);
            fire.transform.localPosition = new Vector2(radius * Mathf.Cos(angle * i * Mathf.Deg2Rad), radius * Mathf.Sin(angle * i * Mathf.Deg2Rad));
            melee.Add(fire);
        }
    }
}
