using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Item",menuName = "Scriptble Object/ItemData")]
public class ItemsScript : ScriptableObject
{
    public enum ItemTypes { FireDamage ,FireSpeed , FireRange, FireCount , PlayerSpeed, PlayerHealth}
    public ItemTypes ItemType;
    public int id;
    public float[] Value;
    public int lvl =0;

    public float baseDamage = 10;
    public float baseSpeed = 150;
    public float baseRange = 1;
    public float baseCount = 2;

}
