using PlayFab;
using PlayFab.ClientModels;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using static UnityEditor.Progress;

public class PlayfabManager : MonoBehaviour
{
    public List<SkinsData> skinsdata;

    public static PlayfabManager Instance { get; set; }

    public GameObject rowPlayer;
    public Transform rowParent;

    public Transform itemsShop;
    int score;
    string loggedPlayer;
    void Start()
    {
        Instance = this;
        
    }


    public TMP_InputField EmailLogin;
    public TMP_InputField PasswordLogin;
    public TMP_InputField EmailRgister;
    public TMP_InputField UsernameRegister;
    public TMP_InputField PasswordRegister;
    public TextMeshProUGUI RegistorOrLoginError;
    public TextMeshProUGUI LoginInfo;
    public TextMeshProUGUI playerName;
    public TextMeshProUGUI coinsShop;


    public GameObject mainMenu;
    public void LoginPlayFab()
    {

        var request = new LoginWithEmailAddressRequest 
        {
            Email = EmailLogin.text,
            Password = PasswordLogin.text,

            InfoRequestParameters = new GetPlayerCombinedInfoRequestParams
            {
                GetPlayerProfile = true,
            }
        };
        PlayFabClientAPI.LoginWithEmailAddress(request, SuccesLogin, ErrorsRequest);
        

    }

    public void RegisterPlayFab()
    {
        var PasswordRequest = new RegisterPlayFabUserRequest
        {
            Email = EmailRgister.text,
            Username = UsernameRegister.text,
            Password = PasswordRegister.text,
            
            RequireBothUsernameAndEmail = false
        };
        PlayFabClientAPI.RegisterPlayFabUser(PasswordRequest, (result) =>
        {
            
            RegistorOrLoginError.text = "Successfull Registered";
            SubmitName(UsernameRegister.text);
            MainMenuScript.instance.registerSuccessful();

        }, (result) =>
        {
            if(PasswordRegister.text.Length < 6)
            {
                RegistorOrLoginError.text = "Password to short";
            }
            else
            {
                RegistorOrLoginError.text = "Error on Registration";
            }
        });
    }

    void ErrorsRequest(PlayFabError error)
    {
        Debug.Log(error);
    }

    void SuccesLogin(LoginResult result)
    {
        loggedPlayer = result.PlayFabId;

        GetCoins();
        GetItemPrice();
        GetEquipedSkin();
        MainMenuScript.instance.LoginButton();

        string name = null;
        if(result.InfoResultPayload.PlayerProfile != null)
        {
            name = result.InfoResultPayload.PlayerProfile.DisplayName;
            
        }
    }

    public void SubmitName(string name)
    {
        Debug.Log(name);
        var request = new UpdateUserTitleDisplayNameRequest
        {
            DisplayName = name,
        };
        PlayFabClientAPI.UpdateUserTitleDisplayName(request, null, ErrorsRequest);
    }

    public void SendBestScores(int score)
    {
        var request = new GetPlayerStatisticsRequest();
        PlayFabClientAPI.GetPlayerStatistics(request, (result) =>
        {
            int bestScore = 0;

            var stat = result.Statistics.Find(s => s.StatisticName == "BestScores");
            if (stat != null)
            {
                bestScore = stat.Value;
            }

            if (score > bestScore)
            {
                var updateRequest = new UpdatePlayerStatisticsRequest
                {
                    Statistics = new List<StatisticUpdate>
                {
                    new StatisticUpdate
                    {
                        StatisticName = "BestScores",
                        Value = score
                    }
                }
                };
                PlayFabClientAPI.UpdatePlayerStatistics(updateRequest, null, ErrorsRequest);
            }
        }, ErrorsRequest);
    }


    public void LeaderBoardGetData()
    {
        var request = new GetLeaderboardRequest
        {
            StatisticName = "BestScores",
            StartPosition = 0,
            MaxResultsCount = 20
        };
        PlayFabClientAPI.GetLeaderboard(request, (result) =>
        {
            foreach (Transform data in rowParent)
            {
                Destroy(data.gameObject);
            }

            foreach (var data in result.Leaderboard)
            {
                GameObject newPlayer = Instantiate(rowPlayer, rowParent);
                TextMeshProUGUI[] texts = newPlayer.GetComponentsInChildren<TextMeshProUGUI>();
                texts[0].text = (data.Position + 1).ToString();
                texts[1].text = data.DisplayName;
                texts[2].text = data.StatValue.ToString();

                if (data.PlayFabId == loggedPlayer)
                {
                    texts[0].color = Color.yellow;
                    texts[1].color = Color.yellow;
                    texts[2].color = Color.yellow;
                }
            }
        }, ErrorsRequest);
    }

    public void GetCoins()
    {
        PlayFabClientAPI.GetUserInventory(new GetUserInventoryRequest(), (result) =>
        {
            coinsShop.text = result.VirtualCurrency["MN"].ToString();
        }, ErrorsRequest);
    }


   public void GetItemPrice()
   {
        GetCatalogItemsRequest request = new GetCatalogItemsRequest();
        request.CatalogVersion = "Skins";
        PlayFabClientAPI.GetCatalogItems(request,result =>{
            List<CatalogItem> items = result.Catalog;
            foreach (CatalogItem i in items)
            {
                int price = (int)i.VirtualCurrencyPrices["MN"];

                foreach (SkinsData data in skinsdata)
                {

                    if (data.id.ToString() == i.ItemId)
                    {
                        data.price = price;
                        CheclIfInInventoryAlready(data.id.ToString());
                    }

                    
                }
            }

            foreach (SkinsData d in skinsdata)
            {
                d.transform.GetChild(1).GetComponent<Button>().onClick.AddListener(delegate { makePurchase(d.id.ToString(), d.price); });
            }
           

            },onErrorGetItems);
   }

    void onErrorGetItems(PlayFabError error)
    {
        Debug.Log(error);
    }

    public void makePurchase(string id, int price)
    {
        PurchaseItemRequest purchaseItemRequest = new PurchaseItemRequest();
        purchaseItemRequest.CatalogVersion = "Skins";
        purchaseItemRequest.ItemId = id;
        purchaseItemRequest.Price = price;
        purchaseItemRequest.VirtualCurrency = "MN";
        
        PlayFabClientAPI.PurchaseItem(purchaseItemRequest, result =>
        {
            GetItemPrice();
            GetCoins();
        }, OnErrorPurchase);
        
    }

    void OnErrorPurchase(PlayFabError error)
    {
        Debug.Log(error);
    }

    public void CheclIfInInventoryAlready(string id)
    {
        PlayFabClientAPI.GetUserInventory(new GetUserInventoryRequest(), (result) =>
        {

            bool itemExists = result.Inventory.Exists(item => item.ItemId == id);

            if (itemExists)
            {
                foreach (SkinsData i in skinsdata)
                {
                    if (i.id.ToString() == id)
                    {
                        i.isBought = true;
                    }

                }

            }
        }, OnGetUserInventoryFailure);
    }

    private void OnGetUserInventoryFailure(PlayFabError error)
    {
        Debug.Log(error.GenerateErrorReport());
    }

    public PlayerViewSkinsScript PlayerViewSkinsScript;

    public void GetEquipedSkin()
    {
        PlayFabClientAPI.GetUserData(new GetUserDataRequest(),(result) =>{
            if(result.Data != null)
            {
                if(result.Data.ContainsKey("PlayerSkin") && result.Data.ContainsKey("WeaponSkin"))
                {
                    PlayerViewSkinsScript.setSkins(result.Data["PlayerSkin"].Value, result.Data["WeaponSkin"].Value);
                }
                else
                {
                    PlayerViewSkinsScript.setSkins("5", "1");
                }
            }
        },ErrorsRequest);
    }

    public void SavePlayerSkinData(string playerSkinId,string weaponSkinId)
    {
        var request = new UpdateUserDataRequest
        {
            Data = new Dictionary<string, string>
            {
                { "PlayerSkin", playerSkinId },
                { "WeaponSkin", weaponSkinId }
            }
        };
        PlayFabClientAPI.UpdateUserData(request, null, ErrorsRequest);
    }
}
