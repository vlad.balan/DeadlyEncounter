using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;
using Random = UnityEngine.Random;

public class Spawner : MonoBehaviour
{
    public static Spawner instance { get; set; }
    public float spawnDelay = 1f;
    public float spawnRadius = 10f;
    public SpawnData[] spawnData;

    public bool spawnOn_Off = false;

    private float spawnTimer;
    private int level;
    private Camera mainCamera;

    public Transform enemyContainer;

    private void Start()
    {
        instance = this;

        mainCamera = Camera.main;
    }


    private void Update()
    {
        if (spawnOn_Off)
        {
            spawnTimer += Time.deltaTime;
            level = Mathf.Min(Mathf.FloorToInt(GameManager.instance.gameTime / 10f), spawnData.Length - 1);

            if (spawnTimer > spawnData[level].spawnTime)
            {
                spawnTimer = 0;
                SpawnEnemy();
            }
        }
    }

    private void SpawnEnemy()
    {
        GameObject enemyPrefab = spawnData[level].enemyPrefab;
        Vector2 spawnPosition = GetRandomSpawnPosition();
        Instantiate(enemyPrefab, spawnPosition, Quaternion.identity, enemyContainer);
        enemyPrefab.GetComponent<Enemy>().Init(spawnData[level]);
        
    }

    private Vector2 GetRandomSpawnPosition()
    {

        float cameraHeight = 2f * mainCamera.orthographicSize;
        float cameraWidth = cameraHeight * mainCamera.aspect;
        float leftBound = mainCamera.transform.position.x - cameraWidth / 2f;
        float rightBound = mainCamera.transform.position.x + cameraWidth / 2f;
        float bottomBound = mainCamera.transform.position.y - cameraHeight / 2f;
        float topBound = mainCamera.transform.position.y + cameraHeight / 2f;

        Vector2 spawnPosition = Vector2.zero;
        float spawnOffset = spawnRadius + 1f; 
        int spawnSide = Random.Range(0, 4); 
        switch (spawnSide)
        {
            case 0: // left
                spawnPosition.x = leftBound - spawnOffset;
                spawnPosition.y = Random.Range(bottomBound, topBound);
                break;
            case 1: // right
                spawnPosition.x = rightBound + spawnOffset;
                spawnPosition.y = Random.Range(bottomBound, topBound);
                break;
            case 2: // top
                spawnPosition.x = Random.Range(leftBound, rightBound);
                spawnPosition.y = topBound + spawnOffset;
                break;
            case 3: // bottom
                spawnPosition.x = Random.Range(leftBound, rightBound);
                spawnPosition.y = bottomBound - spawnOffset;
                break;
        }

        return spawnPosition;
    }

    public void DeleteAllEnemies()
    {
        
        foreach (Transform child in transform)
        {

            Enemy enemy = child.GetComponent<Enemy>();
            if (enemy != null)
            {

                Destroy(enemy.gameObject);
            }
        }
    }
}



[System.Serializable]
public class SpawnData
{
    public GameObject enemyPrefab;
    public int spriteType;
    public float spawnTime;
    public int health;
    public float speed;
    public float size;
    public float damage;
    public int coins;
}
